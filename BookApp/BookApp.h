#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_BookApp.h"
#include <vector>

class Kniha
{
private:
	QString nazov;
	QString autor;
	int ID;
	double nakupna_cena;
public:
	Kniha() { 	}
	Kniha(QString nazov, QString autor, int ID, double nakupna_cena) { this->nazov = nazov; this->autor = autor; this->ID = ID;	this->nakupna_cena = nakupna_cena; }
	~Kniha() {	}
	QString showNazov() { return nazov; }
	QString showAutor() { return autor; }
	int showID() { return ID; }
	double showNakupnaCena() { return nakupna_cena; }
};

class Produkt
{
protected:
	int ID;
	double predajna_cena;
	QString odkial;
	QString nazov;
private:
	int pocet_knih;
public:
	Produkt() { 	}
	Produkt(int ID, double predajna_cena, int pocet_knih, QString odkial, QString nazov);
	void decrementPocet() { pocet_knih--; }
	~Produkt() {	}
	int showID() { return ID; }
	double showPrice() { return predajna_cena; }
	int showPocet() { return pocet_knih; }
	QString Odkial() { return odkial; }
	QString Nazov() { return nazov; }
	bool operator== (Produkt& P);
	int howMuchLeft(int count);
};

class Knihkupectvo
{
private:
	QString nazov;
	std::vector<Produkt> knihy;
	double zisk = 0.;
	int bookCount;
	int items_sold = 0;
public:
	Knihkupectvo() { }
	Knihkupectvo(QString nazov, std::vector<Produkt> products);
	~Knihkupectvo() {	}
	QString showName() { return nazov; }
	Produkt showProdukt(int j) { return knihy[j]; }
	void addZisk(double zarobok, int ID, std::vector<Kniha> &predavane);
	double showZisk() { return zisk; }
	int showBookCount() { return bookCount;  }
	int itemsSold() { return items_sold;  }
};


class ZakupenyProdukt : public Produkt
{
private:
	QString meno;
public:
	ZakupenyProdukt() { }
	ZakupenyProdukt(int ID, double predajna_cena, QString odkial, QString nazov);
	void setName(QString name) { meno = name; }
	~ZakupenyProdukt() { }
	QString showZakupeny();
};

class Adresa
{
private:
	QString ulica;
	QString mesto;
	int supisne_cislo;
	int psc;
public:
	Adresa() { }
	Adresa(QString ulica, int c, int psc, QString mesto);
	~Adresa() { }
	QString Ulica() { return ulica;  }
	QString Mesto() { return mesto;  }
	int Supisne_cislo() { return supisne_cislo; }
	int Psc() { return psc; }
};

class Kosik
{
private:
	QList<Produkt> items;
	double toPay = 0;
public:
	Kosik() { }
	~Kosik() { }
	void AddItem(Produkt item);
	void RemoveItem(int i);
	void addToPay(double added) { toPay += added; }
	void RemoveAll();
	Produkt showItem(int i) { return items[i]; }
	double howMuch() { return toPay; }
	int Size() { return items.size(); }
	int howManyTimes(Produkt P);
	bool isEmpty();
};


class Zakaznik
{
private:
	QString meno;
	QString priezvisko;
	Adresa mailing_addr;
	Adresa res_addr;
	double rozpocet;
	Kosik my_cart;
	std::vector<ZakupenyProdukt> kupene;
public:
	Zakaznik(QString meno, QString priezvisko, Adresa mailing_addr, Adresa res_addr, double rozpocet);
	~Zakaznik() {	}
	QString showMeno() { return meno; }
	QString showPriezvisko() { return priezvisko; }
	QString showAddress(bool ismailing);
	Kosik& showKosik() { return my_cart;  }
	double showRozpocet() { return rozpocet; }
	ZakupenyProdukt showKupene(int i) { return kupene[i]; }
	int kupeneSize() { return kupene.size(); }
	double overallSpent();
	void buy();
};


class BookApp : public QMainWindow
{
	Q_OBJECT

public:
	BookApp(QWidget *parent = Q_NULLPTR);
	void LoadDatabase();
	std::vector<Kniha> knihy;
	std::vector<Knihkupectvo> knihkupectva;
	void bookstoresRevenue(Kosik *k);
	bool isDeselected(QListWidget *L);

public slots:
	void ActionSignIn();
	void ActionChangeUser();
	void ActionSearch();
	void ActionAddToCart();
	void ActionRemoveFromCart();
	void ActionRemoveAll();
	void ActionBuy();
	
	void EnableMinPrice();
	void EnableMaxPrice();
	void EnableMailingAddress();

	void MinPriceChanged();
	void MaxPriceChanged();

	void ActionWriteReports();

private:
	Ui::BookAppClass ui;
	Zakaznik *logged = NULL;
	std::vector<Zakaznik> users;
	QListWidgetItem *cartable;
	double min_price = 0;
	double max_price = 300.;
	bool price_filter_low = false;
	bool price_filter_up = false;
};
