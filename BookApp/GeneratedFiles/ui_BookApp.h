/********************************************************************************
** Form generated from reading UI file 'BookApp.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKAPP_H
#define UI_BOOKAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BookAppClass
{
public:
    QWidget *centralWidget;
    QFormLayout *formLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QHBoxLayout *horizontalLayout_2;
    QSlider *horizontalSlider;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_3;
    QSlider *horizontalSlider_2;
    QLabel *label_6;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_2;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QListWidget *listWidget;
    QPushButton *pushButton_2;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QListWidget *listWidget_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QHBoxLayout *horizontalLayout;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_7;
    QLabel *label_8;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_3;
    QPushButton *pushButton_7;
    QLabel *label_5;
    QLabel *label_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_2;
    QCheckBox *checkBox;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton_8;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_5;
    QLabel *label_11;
    QPushButton *pushButton_6;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *BookAppClass)
    {
        if (BookAppClass->objectName().isEmpty())
            BookAppClass->setObjectName(QStringLiteral("BookAppClass"));
        BookAppClass->resize(712, 607);
        centralWidget = new QWidget(BookAppClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        formLayout = new QFormLayout(centralWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        gridLayout->addWidget(lineEdit, 0, 0, 1, 1);

        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setEnabled(false);

        gridLayout->addWidget(pushButton, 0, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSlider = new QSlider(groupBox);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setEnabled(false);
        horizontalSlider->setMaximum(600);
        horizontalSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);


        gridLayout->addLayout(horizontalLayout_2, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSlider_2 = new QSlider(groupBox);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setEnabled(false);
        horizontalSlider_2->setMinimum(0);
        horizontalSlider_2->setMaximum(600);
        horizontalSlider_2->setValue(600);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(horizontalSlider_2);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_3->addWidget(label_6);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 1);

        checkBox_3 = new QCheckBox(groupBox);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setLayoutDirection(Qt::RightToLeft);

        gridLayout->addWidget(checkBox_3, 1, 1, 1, 1);

        checkBox_2 = new QCheckBox(groupBox);
        checkBox_2->setObjectName(QStringLiteral("checkBox_2"));
        checkBox_2->setLayoutDirection(Qt::RightToLeft);

        gridLayout->addWidget(checkBox_2, 2, 1, 1, 1);


        formLayout->setWidget(0, QFormLayout::SpanningRole, groupBox);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(87, 13));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        listWidget = new QListWidget(groupBox_2);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        gridLayout_2->addWidget(listWidget, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(groupBox_2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setEnabled(false);

        gridLayout_2->addWidget(pushButton_2, 1, 0, 1, 1);


        formLayout->setWidget(1, QFormLayout::LabelRole, groupBox_2);

        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        listWidget_2 = new QListWidget(groupBox_3);
        listWidget_2->setObjectName(QStringLiteral("listWidget_2"));

        verticalLayout_2->addWidget(listWidget_2);

        pushButton_3 = new QPushButton(groupBox_3);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setEnabled(false);

        verticalLayout_2->addWidget(pushButton_3);

        pushButton_4 = new QPushButton(groupBox_3);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setEnabled(false);

        verticalLayout_2->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(groupBox_3);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setEnabled(false);

        verticalLayout_2->addWidget(pushButton_5);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout->addWidget(label_9);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout->addWidget(label_10);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));

        horizontalLayout->addWidget(label_7);

        label_8 = new QLabel(groupBox_3);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout->addWidget(label_8);


        verticalLayout_2->addLayout(horizontalLayout);


        formLayout->setWidget(1, QFormLayout::FieldRole, groupBox_3);

        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridLayout_3 = new QGridLayout(groupBox_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        pushButton_7 = new QPushButton(groupBox_4);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        gridLayout_3->addWidget(pushButton_7, 4, 0, 1, 1);

        label_5 = new QLabel(groupBox_4);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 3, 0, 1, 1);

        label_3 = new QLabel(groupBox_4);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 2, 0, 1, 1);

        lineEdit_4 = new QLineEdit(groupBox_4);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setEnabled(false);

        gridLayout_3->addWidget(lineEdit_4, 3, 1, 1, 1);

        lineEdit_2 = new QLineEdit(groupBox_4);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));

        gridLayout_3->addWidget(lineEdit_2, 1, 1, 1, 1);

        checkBox = new QCheckBox(groupBox_4);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setChecked(true);
        checkBox->setAutoRepeat(false);

        gridLayout_3->addWidget(checkBox, 3, 2, 1, 1);

        label = new QLabel(groupBox_4);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 1, 0, 1, 1);

        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 2, 2, 1, 1);

        pushButton_8 = new QPushButton(groupBox_4);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setEnabled(false);

        gridLayout_3->addWidget(pushButton_8, 4, 2, 1, 1);

        lineEdit_3 = new QLineEdit(groupBox_4);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));

        gridLayout_3->addWidget(lineEdit_3, 2, 1, 1, 1);

        lineEdit_5 = new QLineEdit(groupBox_4);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        gridLayout_3->addWidget(lineEdit_5, 1, 2, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_3->addWidget(label_11, 0, 2, 1, 1);


        formLayout->setWidget(2, QFormLayout::SpanningRole, groupBox_4);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setEnabled(false);

        formLayout->setWidget(5, QFormLayout::FieldRole, pushButton_6);

        BookAppClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(BookAppClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 712, 21));
        BookAppClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(BookAppClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        BookAppClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(BookAppClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        BookAppClass->setStatusBar(statusBar);

        retranslateUi(BookAppClass);
        QObject::connect(pushButton_7, SIGNAL(clicked()), BookAppClass, SLOT(ActionSignIn()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), BookAppClass, SLOT(ActionChangeUser()));
        QObject::connect(pushButton, SIGNAL(clicked()), BookAppClass, SLOT(ActionSearch()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), BookAppClass, SLOT(ActionAddToCart()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), BookAppClass, SLOT(ActionRemoveFromCart()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), BookAppClass, SLOT(ActionRemoveAll()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), BookAppClass, SLOT(ActionBuy()));
        QObject::connect(checkBox_3, SIGNAL(toggled(bool)), BookAppClass, SLOT(EnableMinPrice()));
        QObject::connect(checkBox_2, SIGNAL(toggled(bool)), BookAppClass, SLOT(EnableMaxPrice()));
        QObject::connect(checkBox, SIGNAL(toggled(bool)), BookAppClass, SLOT(EnableMailingAddress()));
        QObject::connect(horizontalSlider, SIGNAL(valueChanged(int)), BookAppClass, SLOT(MinPriceChanged()));
        QObject::connect(horizontalSlider_2, SIGNAL(valueChanged(int)), BookAppClass, SLOT(MaxPriceChanged()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), BookAppClass, SLOT(ActionWriteReports()));

        QMetaObject::connectSlotsByName(BookAppClass);
    } // setupUi

    void retranslateUi(QMainWindow *BookAppClass)
    {
        BookAppClass->setWindowTitle(QApplication::translate("BookAppClass", "BookApp", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("BookAppClass", "Find books ( title/author):", Q_NULLPTR));
        pushButton->setText(QApplication::translate("BookAppClass", "Search", Q_NULLPTR));
        label_4->setText(QApplication::translate("BookAppClass", "0", Q_NULLPTR));
        label_6->setText(QApplication::translate("BookAppClass", "300", Q_NULLPTR));
        checkBox_3->setText(QApplication::translate("BookAppClass", "Min. price", Q_NULLPTR));
        checkBox_2->setText(QApplication::translate("BookAppClass", "Max price", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("BookAppClass", "Search query:", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("BookAppClass", "Add to cart", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("BookAppClass", "Shopping cart:", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("BookAppClass", "Remove item from cart", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("BookAppClass", "Remove all items", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("BookAppClass", "Buy", Q_NULLPTR));
        label_9->setText(QApplication::translate("BookAppClass", "To Pay:", Q_NULLPTR));
        label_10->setText(QApplication::translate("BookAppClass", "0.00", Q_NULLPTR));
        label_7->setText(QApplication::translate("BookAppClass", "Budget:", Q_NULLPTR));
        label_8->setText(QApplication::translate("BookAppClass", "0.00", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("BookAppClass", "User info:", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("BookAppClass", "Sign in", Q_NULLPTR));
        label_5->setText(QApplication::translate("BookAppClass", "Mailing Address:", Q_NULLPTR));
        label_3->setText(QApplication::translate("BookAppClass", "Residential Address:", Q_NULLPTR));
        lineEdit_4->setText(QApplication::translate("BookAppClass", "Lubovnikova 42, 84107, Bratislava", Q_NULLPTR));
        lineEdit_2->setText(QApplication::translate("BookAppClass", "Martin Cavarga", Q_NULLPTR));
        checkBox->setText(QApplication::translate("BookAppClass", "Same as residential addr.", Q_NULLPTR));
        label->setText(QApplication::translate("BookAppClass", "Name:", Q_NULLPTR));
        label_2->setText(QApplication::translate("BookAppClass", "format: Street number, Zip, City", Q_NULLPTR));
        pushButton_8->setText(QApplication::translate("BookAppClass", "Change user", Q_NULLPTR));
        lineEdit_3->setText(QApplication::translate("BookAppClass", "Lubovnikova 42, 84107, Bratislava", Q_NULLPTR));
        lineEdit_5->setText(QApplication::translate("BookAppClass", "100", Q_NULLPTR));
        label_11->setText(QApplication::translate("BookAppClass", "Starting Budget [EUR]:", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("BookAppClass", "Finish and write reports", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class BookAppClass: public Ui_BookAppClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKAPP_H
