#include "BookApp.h"
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QListWidgetItem>
#include <QRegExp>
#include <QDebug>
#include <set>
#define nofiles -1

BookApp::BookApp(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	LoadDatabase();
}

void BookApp::ActionSignIn()
{
	if (ui.lineEdit_2->text()!=QStringLiteral("") 
		&& ui.lineEdit_3->text() != QStringLiteral("") 
		&& ui.lineEdit_4->text() != QStringLiteral("")
		&& ui.lineEdit_5->text() != QStringLiteral(""))
	{
		QString Name = ui.lineEdit_2->text().split(" ").value(0);
		QString Surname = ui.lineEdit_2->text().split(" ").value(1);
		QString text_residential = ui.lineEdit_3->text();
		QString text_mailing = ui.lineEdit_4->text();
		double budget = ui.lineEdit_5->text().toDouble();

		QString text_streetname = text_residential.split(",").value(0);

		int wordsize = text_streetname.split(" ").size();
		QString streetname_beginning = QString("");
		for (int i = 0; i < wordsize - 1; i++) streetname_beginning += text_streetname.split(" ").value(i);

		Adresa residential =  Adresa(streetname_beginning, text_streetname.split(" ").value(wordsize - 1).toInt() ,
			text_residential.split(",").value(1).toInt(), text_residential.split(",").value(2));

		text_streetname = text_mailing.split(",").value(0);

		wordsize = text_streetname.split(" ").size();
		streetname_beginning = QString("");
		for (int i = 0; i < wordsize - 1; i++) streetname_beginning += text_streetname.split(" ").value(i);

		Adresa mailing = Adresa(streetname_beginning, text_streetname.split(" ").value(wordsize - 1).toInt(),
			text_mailing.split(",").value(1).toInt(), text_mailing.split(",").value(2));
		
		logged = new Zakaznik(Name,Surname,mailing,residential,budget);

		ui.label_8->setText(QString::number(budget));

		ui.lineEdit_2->setEnabled(false);
		ui.lineEdit_3->setEnabled(false);
		ui.lineEdit_5->setEnabled(false);
		ui.pushButton_7->setEnabled(false);
		ui.checkBox->setEnabled(false);

		ui.pushButton_8->setEnabled(true);
		ui.pushButton->setEnabled(true);
		ui.pushButton_6->setEnabled(true);
		ui.pushButton_2->setEnabled(true);
		ui.pushButton_3->setEnabled(true);
		ui.pushButton_4->setEnabled(true);
		ui.pushButton_5->setEnabled(true);
	}
	else
	{
		QMessageBox msgBox;
		msgBox.setText(QStringLiteral("Please enter all your credentials"));
		msgBox.exec();
	}
}

Knihkupectvo::Knihkupectvo(QString nazov, std::vector<Produkt> products)
{
	this->nazov = nazov;
	this->bookCount = bookCount;

	int bookCount = products.size();

	knihy.reserve(bookCount);
	copy(products.begin(), products.end(), back_inserter(knihy));
}

void Knihkupectvo::addZisk(double zarobok, int ID, std::vector<Kniha> &predavane)
{
	double povodna;
	for (int i = 0; i < predavane.size(); i++)
	{
		if (predavane[i].showID() == ID) povodna = predavane[i].showNakupnaCena();
	}
	zisk += zarobok - povodna;
	for (int i = 0; i < knihy.size(); i++)
	{
		if (knihy[i].showID() == ID)
		{
			knihy[i].decrementPocet();
			items_sold++;
			continue;
		}
	}
}


Produkt::Produkt(int ID, double predajna_cena, int pocet_knih, QString odkial, QString nazov)
{
	this->ID = ID;
	this->predajna_cena = predajna_cena;
	this->pocet_knih = pocet_knih;
	this->odkial = odkial;
	this->nazov = nazov;
}

bool Produkt::operator==(Produkt & P)
{
	return (ID == P.ID && predajna_cena == P.predajna_cena && odkial == P.odkial);
}

int Produkt::howMuchLeft(int count)
{
	return pocet_knih - count;
}

ZakupenyProdukt::ZakupenyProdukt(int ID, double predajna_cena, QString odkial, QString nazov)
{
	this->ID = ID;
	this->predajna_cena = predajna_cena;
	this->odkial = odkial;
	meno = nazov;
}

QString ZakupenyProdukt::showZakupeny()
{
	return QString::number(ID) + QString(" ") + meno + QString(", ") 
		+ odkial + QString(", paid: ") + QString::number(predajna_cena) + QString(" EUR");
}

Zakaznik::Zakaznik(QString meno, QString priezvisko, Adresa mailing_addr, Adresa res_addr, double rozpocet)
{
	this->meno = meno; this->priezvisko = priezvisko; this->rozpocet = rozpocet; 
	this->mailing_addr = mailing_addr; this->res_addr = res_addr;
	my_cart = Kosik();
}

QString Zakaznik::showAddress(bool ismailing)
{
	if (ismailing)
	{
		return (mailing_addr.Ulica() + QString(" ") + QString::number(mailing_addr.Supisne_cislo()) + QString(" ,")
			+ QString::number(mailing_addr.Psc()) + QString(" ,") + mailing_addr.Mesto());
		qDebug() << "printing mailing addr.";
	}
	else
	{
		return (res_addr.Ulica() + QString(" ") + QString::number(res_addr.Supisne_cislo()) + QString(" ,")
			+ QString::number(res_addr.Psc()) + QString(" ,") + res_addr.Mesto());
		qDebug() << "printing residential addr.";
	}
}

double Zakaznik::overallSpent()
{
	double spent = 0.;
	for (int i = 0; i < kupene.size(); i++)
	{
		spent += kupene[i].showPrice();
	}
	return spent;
}

void Zakaznik::buy()
{
	for (int i = 0; i < my_cart.Size(); i++)
	{
		kupene.push_back(ZakupenyProdukt(my_cart.showItem(i).showID(),my_cart.showItem(i).showPrice(),
			my_cart.showItem(i).Odkial(), my_cart.showItem(i).Nazov()));
		rozpocet -= my_cart.showItem(i).showPrice();		
	}
}

void BookApp::ActionWriteReports()
{
	bool write = false;
	if (logged != NULL && users.empty())
	{
		users.push_back(*logged);

		write = true;
	}
	else if (logged == NULL && !users.empty())
	{
		write = true;
	}
	else if (logged != NULL && !users.empty())
	{
		users.push_back(*logged);

		write = true;
	}
	if (write)
	{
		QFile report1(QString("CustomerReport.txt"));
		QFile report2(QString("StoreReport.txt"));

		if (report1.open(QIODevice::ReadWrite) && report2.open(QIODevice::ReadWrite))
		{
			report1.resize(0); report2.resize(0);
			QTextStream customerData(&report1);
			QTextStream storeData(&report2);

			QString header = QString("-------------------------------------------------") + "\n" 
				+ QString("F U L L    C U S T O M E R    R E P O R T") + "\n"
				+ QString("-------------------------------------------------") + "\n"
				+ QString("-------------------------------------------------") + "\n";

			customerData << header;
			QString line;
			for (int i = 0; i < users.size(); i++)
			{
				line = QString("User") + QString::number(i + 1) + QString(": ") 
					+ users[i].showMeno() + QString(" ") + users[i].showPriezvisko() + "\n";
				customerData << line;

				line = QString("Mailing address: ") + users[i].showAddress(true) + "\n";
				customerData << line;

				line = QString("Residential address: ") + users[i].showAddress(false) + "\n";
				customerData << line;

				line = QString(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .") + "\n";
				customerData << line;

				line = QString("Purchased items: ") + "\n";
				customerData << line;
				for (int j = 0; j < users[i].kupeneSize(); j++)
				{
					line = QString("(") + QString::number(j + 1) + QString(") ")
						+ users[i].showKupene(j).showZakupeny() + "\n";
					customerData << line;
				}
				line = QString(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .") + "\n";
				customerData << line;
				line = QString("Overall spent : ") + QString::number(users[i].overallSpent()) + QString(" EUR") + "\n";
				customerData << line;
				line = QString("-------------------------------------------------") + "\n";
				customerData << line;
			}

			header = QString("#############################################################") + "\n"
				+ QString("#######^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^#######") + "\n"
				+ QString("#######  F U L L    B O O K S T O R E    R E P O R T  #######") + "\n"
				+ QString("#######vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv#######") + "\n"
				+ QString("#############################################################") + "\n";

			storeData << header;

			for (int i = 0; i < knihkupectva.size(); i++)
			{
				line = QString("Name: ") + knihkupectva[i].showName() + "\n";
				storeData << line;
				line = QString("overall revenue:               ")
					+ QString::number(knihkupectva[i].showZisk()) + QString(" EUR") + "\n";
				storeData << line;
				line = QString("total # of items sold:               ")
					+ QString::number(knihkupectva[i].itemsSold()) + "\n";
				storeData << line;
				line = QString("-------------------------------------------------------------------------") + "\n";
				storeData << line;
			}

			report1.close();
			report2.close();

			close();
		}
		else
		{
			ui.statusBar->show();
			ui.statusBar->showMessage(QStringLiteral("Unable to save report files!"));
			QMessageBox msgBox;
			msgBox.setText(QStringLiteral("Unable to save report files! Possibly not enough space."));
			msgBox.exec();
		}
	}
}


void BookApp::LoadDatabase()
{
	try
	{
		QString filename1 = QStringLiteral("Knihkupectva.txt");
		QString filename2 = QStringLiteral("Knihy.txt");
		QFile file1(filename1); QFile file2(filename2);
		if (!file2.open(QIODevice::ReadOnly) || !file1.open(QIODevice::ReadOnly)) throw nofiles;

		QTextStream shopdata(&file1);
		QTextStream bookdata(&file2);
		QString line;
		
		line = shopdata.readLine(100);
		int shopCount = line.toInt();
		line = bookdata.readLine(100);
		int bookCount = line.toInt();

		QString title, author;
		double price;
		int ID;

		for (int i = 0; i < bookCount; i++)
		{
			title = bookdata.readLine(200);
			author = bookdata.readLine(200);
			ID = bookdata.readLine(100).toInt();
			price = bookdata.readLine(100).toDouble();
			knihy.push_back(Kniha(title, author, ID, price));
		}

		file2.close();

		QString name, line2, line3;
		std::vector<Produkt> products;
		
		for (int i = 0; i < shopCount; i++)
		{
			name = shopdata.readLine(100);
			line2 = shopdata.readLine(1000);
			line3 = shopdata.readLine(1000);
			for (int j = 0; j < bookCount; j++)
			{
				products.push_back(Produkt(knihy[j].showID(), line2.split(" ").value(j).toDouble(),
					line3.split(" ").value(j).toInt(),name, (knihy[j].showNazov() + QString(" - ") + knihy[j].showAutor()) ));
			}
			knihkupectva.push_back(Knihkupectvo(name, products));
			products.clear();
		}

		file1.close();
	}
	catch (int)
	{
		if (nofiles == -1)
		{
			ui.statusBar->show();
			ui.statusBar->showMessage(QStringLiteral("Unable to open files Knihkupectva.txt and Knihy.txt!"));
			QMessageBox msgBox;
			msgBox.setText(QStringLiteral("Unable to open files Knihkupectva.txt or Knihy.txt! Data files must be added into source folder BookApp/BookApp."));
			msgBox.exec();
		}
	}
}

void BookApp::bookstoresRevenue(Kosik *k)
{
	for (int i = 0; i < k->Size(); i++)
	{
		for (int j = 0; j < knihkupectva.size(); j++)
		if (k->showItem(i).Odkial() == knihkupectva[j].showName())
		{
			knihkupectva[j].addZisk(k->showItem(i).showPrice(), k->showItem(i).showID(), knihy);
			continue;
		}
	}
}

bool BookApp::isDeselected(QListWidget *L)
{
	bool result = true;
	for (int i = 0; i < L->count(); i++)
	{
		if (L->item(i)->isSelected())
		{
			result = false;
			continue;
		}
	}
	return result;
}

void BookApp::ActionChangeUser()
{
	users.push_back(*logged);
	delete logged;
	logged = NULL;

	ui.label_8->setText(QStringLiteral("0.00"));

	ui.lineEdit_2->setText(QStringLiteral(""));
	ui.lineEdit_3->setText(QStringLiteral(""));
	ui.lineEdit_4->setText(QStringLiteral(""));
	ui.lineEdit_5->setText(QStringLiteral(""));

	ui.lineEdit_2->setEnabled(true);
	ui.lineEdit_3->setEnabled(true);
	EnableMailingAddress();
	ui.lineEdit_5->setEnabled(true);
	ui.checkBox->setEnabled(true);
	ui.pushButton_7->setEnabled(true);

	ui.pushButton_8->setEnabled(false);
	ui.pushButton_2->setEnabled(false);
	ui.pushButton_3->setEnabled(false);
	ui.pushButton_4->setEnabled(false);
	ui.pushButton_5->setEnabled(false);
}

void BookApp::ActionSearch()
{
	QString searchstr = ui.lineEdit->text();
	ui.listWidget->clear();

	for (int i = 0; i < knihkupectva.size(); i++)
	{
		for (int j = 0; j < knihy.size(); j++)
		{
			if (( knihy[j].showNazov().contains(searchstr, Qt::CaseInsensitive) ||
				knihy[j].showAutor().contains(searchstr, Qt::CaseInsensitive)) &&
				knihkupectva[i].showProdukt(j).showPocet() > 0 && !price_filter_low && !price_filter_up )
			{
				QString item_text = QString::number(knihkupectva[i].showProdukt(j).showPocet()) + QString(" ks. - ") + knihy[j].showNazov() + QString(" - ") + knihy[j].showAutor() + QString(" (")
					+ QString::number(knihkupectva[i].showProdukt(j).showID()) +	QString(") - ")	+ QString::number(knihkupectva[i].showProdukt(j).showPrice())
					+ QString(" EUR - ") + knihkupectva[i].showName();
				ui.listWidget->addItem(item_text);
			}
			else if ((knihy[j].showNazov().contains(searchstr, Qt::CaseInsensitive) ||
				knihy[j].showAutor().contains(searchstr, Qt::CaseInsensitive)) &&
				knihkupectva[i].showProdukt(j).showPocet() > 0 
				&& price_filter_low && !price_filter_up && knihkupectva[i].showProdukt(j).showPrice() >= min_price )
			{
				QString item_text = QString::number(knihkupectva[i].showProdukt(j).showPocet()) + QString(" ks. - ") + knihy[j].showNazov() + QString(" - ") + knihy[j].showAutor() + QString(" (")
					+ QString::number(knihkupectva[i].showProdukt(j).showID()) + QString(") - ") + QString::number(knihkupectva[i].showProdukt(j).showPrice())
					+ QString(" EUR - ") + knihkupectva[i].showName();
				ui.listWidget->addItem(item_text);
			}
			else if ((knihy[j].showNazov().contains(searchstr, Qt::CaseInsensitive) ||
				knihy[j].showAutor().contains(searchstr, Qt::CaseInsensitive)) &&
				knihkupectva[i].showProdukt(j).showPocet() > 0
				&& price_filter_up && !price_filter_low && knihkupectva[i].showProdukt(j).showPrice() <= max_price)
			{
				QString item_text = QString::number(knihkupectva[i].showProdukt(j).showPocet()) + QString(" ks. - ") + knihy[j].showNazov() + QString(" - ") + knihy[j].showAutor() + QString(" (")
					+ QString::number(knihkupectva[i].showProdukt(j).showID()) + QString(") - ") + QString::number(knihkupectva[i].showProdukt(j).showPrice())
					+ QString(" EUR - ") + knihkupectva[i].showName();
				ui.listWidget->addItem(item_text);
			}
			else if ((knihy[j].showNazov().contains(searchstr, Qt::CaseInsensitive) ||
				knihy[j].showAutor().contains(searchstr, Qt::CaseInsensitive)) &&
				knihkupectva[i].showProdukt(j).showPocet() > 0
				&& ( price_filter_low && price_filter_up ) && ( knihkupectva[i].showProdukt(j).showPrice() >= min_price && knihkupectva[i].showProdukt(j).showPrice() <= max_price))
			{
				QString item_text = QString::number(knihkupectva[i].showProdukt(j).showPocet()) + QString(" ks. - ") + knihy[j].showNazov() + QString(" - ") + knihy[j].showAutor() + QString(" (")
					+ QString::number(knihkupectva[i].showProdukt(j).showID()) + QString(") - ") + QString::number(knihkupectva[i].showProdukt(j).showPrice())
					+ QString(" EUR - ") + knihkupectva[i].showName();
				ui.listWidget->addItem(item_text);
			}
		}
	}
}


void BookApp::ActionAddToCart()
{
	if (!isDeselected(ui.listWidget))
	{
		cartable = ui.listWidget->currentItem();
		QString cartedID_text = cartable->text().split(" - ").value(2);
		QRegExp brackets("\\(|\\)");
		int cartedID = cartedID_text.split(brackets).value(1).toInt();
		double cartedPrice = (cartable->text().split(" - ").value(3)).split(" ").value(0).toDouble();;
		int cartedCount = (cartable->text().split(" - ").value(0)).split(" ").value(0).toInt();
		QString from = cartable->text().split(" - ").value(4);
		QString cartedText = cartable->text().split("ks. - ").value(1);

		Produkt carted_product = Produkt(cartedID, cartedPrice, cartedCount, from, (cartable->text().split(" - ").value(1) + QString(" - ") + cartable->text().split(" - ").value(2)));
		bool insertable = true;
		for (int i = 0; i < knihkupectva.size(); i++)
		{
			for (int j = 0; j < knihy.size(); j++)
			{
				if (carted_product == knihkupectva[i].showProdukt(j)
					&& (logged->showKosik().howManyTimes(carted_product) == knihkupectva[i].showProdukt(j).showPocet()))
				{
					insertable = false;
					ui.statusBar->show();
					ui.statusBar->showMessage(QStringLiteral("Out of stock."));
					QMessageBox mesgBox;
					mesgBox.setText(QStringLiteral("There are no more products of this type at the store. Please, select another product if you wish to add more to the shopping cart."));
					mesgBox.exec();
					continue;
				}
			}
		}
		qDebug() << "insertable = " << insertable;
		if (insertable)
		{
			logged->showKosik().AddItem(carted_product);
			logged->showKosik().addToPay(cartedPrice);
			ui.label_10->setText(QString::number(logged->showKosik().howMuch()));
			int lastrow = ui.listWidget_2->count();
			ui.listWidget_2->insertItem(lastrow, cartedText);
		}
	}
}

void BookApp::ActionRemoveFromCart()
{
	if ( !isDeselected(ui.listWidget_2))
	{
		int index = ui.listWidget_2->currentRow();

		double price = -1.*logged->showKosik().showItem(index).showPrice();
		logged->showKosik().RemoveItem(index);
		logged->showKosik().addToPay(price);
		QListWidgetItem *item = ui.listWidget_2->takeItem(index);
		delete item;
		ui.label_10->setText(QString::number(logged->showKosik().howMuch()));
	}
}

void BookApp::ActionRemoveAll()
{
	logged->showKosik().RemoveAll();
	ui.listWidget_2->clear();
	ui.label_10->setText(QString::number(logged->showKosik().howMuch()));
}

void BookApp::ActionBuy()
{
	if (!logged->showKosik().isEmpty())
	{
		if (logged->showKosik().howMuch() > logged->showRozpocet())
		{
			ui.statusBar->show();
			ui.statusBar->showMessage(QStringLiteral("Insufficient funds!"));
			QMessageBox mesgBox;
			mesgBox.setText(QStringLiteral("Insufficient funds!"));
			mesgBox.exec();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setText(QStringLiteral("Are your sure you want to buy ") + QString::number(logged->showKosik().Size()) + QStringLiteral(" items for ") + QString::number(logged->showKosik().howMuch()));
			msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
			msgBox.setDefaultButton(QMessageBox::No);
			int ret = msgBox.exec();

			switch (ret) {
			case QMessageBox::Yes:
			{
				logged->buy();
				bookstoresRevenue(&logged->showKosik());
				ActionRemoveAll();
				ui.label_8->setText(QString::number(logged->showRozpocet()));
				break;
			}
			case QMessageBox::No:
			{
				break;
			}
			default:
				break;
			}
		}
	}
}

void BookApp::EnableMinPrice()
{
	if (ui.checkBox_3->isChecked())
	{
		ui.horizontalSlider->setEnabled(true);
		price_filter_low = true;
	}
	else
	{
		ui.horizontalSlider->setEnabled(false);
		price_filter_low = false;
	}
}

void BookApp::EnableMaxPrice()
{
	if (ui.checkBox_2->isChecked())
	{
		ui.horizontalSlider_2->setEnabled(true);
		price_filter_up = true;
	}
	else
	{
		ui.horizontalSlider_2->setEnabled(false);
		price_filter_up = false;
	}
}

void BookApp::EnableMailingAddress()
{
	if (ui.checkBox->isChecked())
	{
		ui.lineEdit_4->setText(ui.lineEdit_3->text());
		ui.lineEdit_4->setEnabled(false);
	}
	else
	{
		ui.lineEdit_4->setEnabled(true);
	}
}

void BookApp::MinPriceChanged()
{
	min_price = 0.5*ui.horizontalSlider->value();
	ui.label_4->setText(QString::number(min_price));
}

void BookApp::MaxPriceChanged()
{
	max_price = 0.5*ui.horizontalSlider_2->value();
	ui.label_6->setText(QString::number(max_price));
}

Adresa::Adresa(QString ulica, int c, int psc, QString mesto)
{
	this->ulica = ulica;
	supisne_cislo = c; this->psc = psc;
	this->mesto = mesto;
}

void Kosik::AddItem(Produkt item)
{
	items.push_back(item);
}

void Kosik::RemoveItem(int i)
{
	items.removeAt(i);
}

void Kosik::RemoveAll()
{
	items.clear();
	toPay = 0.;
}

int Kosik::howManyTimes(Produkt P)
{
	int count = 0;
	for (int i = 0; i < items.count(); i++)
	{
		if (P == items[i]) count++;
	}
	return count;
}

bool Kosik::isEmpty()
{
	if (items.size() > 0) return false;
	else return true;
}
